$(function(){
    let select = $('#from__select');
    
    let html = ''
    for (let i = 1; i <= 5; i++) {
        html += `<option value=${i}> ${i} год </option>`;
    }
    
    select.append(html);

    function sliderEl(select) {
        let sel = $(select);
        let slider = $(select+'-slider');
    
        slider.slider({
            range: "min",
            value: 1000000,
            step:1000,
            min: 1000,
            max: 3000000,
            slide:function(event, ui){
                sel.val(ui.value);
            }
        })
        sel.change((e)=>{
            slider.slider('value', e.target.value);
        })
    }

    sliderEl('#sum-contribution');
    sliderEl('#sum-contribution-replenishment')
    
    $('#datepicker').datepicker({
        dateFormat: "dd.mm.yy"
    });

    $('#form').on("submit", (e)=>{
        e.preventDefault();
        let data = $('#form').serializeArray();

        let array = {}
        data.map((e)=>{
            array[e.name] = e.value;
        })

        let [day, month, year] = array['deposit-registration-date'].split('.');
        let day_in_mount = new Date(year, month, 0).getDate();

        let day_in_year = 0;
        for (let i = 0; i < array['deposit-term']; i++) {
            new Date(year+i, 1, 29).getMonth() == 1 ? day_in_year += 366 : day_in_year += 365;
        }

        array.daysy = day_in_year;
        array.daysn = day_in_mount;

        data = JSON.stringify(array);
        
        $.ajax({
            url: 'calc.php',
            method: 'POST',
            dataType: 'json',
            data: data,
            success: function(data){
                let sum = Math.round(data);
                console.log(sum);
                let res = $('.result__res-num');
                console.log(res);
                $('.result__res-num').html(sum);
                console.log(res);

            }
        });        
    })
});
